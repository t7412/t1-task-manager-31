package ru.t1.chubarov.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.component.Server;

import java.net.Socket;

public abstract class AbstractServerSocketTask extends AbstractServerTask {

    @NotNull
    protected final Socket socket;


    protected AbstractServerSocketTask(@NotNull final Server server, @NotNull final Socket socket) {
        super(server);
        this.socket = socket;
    }
}
