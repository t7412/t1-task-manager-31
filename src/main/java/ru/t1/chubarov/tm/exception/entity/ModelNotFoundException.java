package ru.t1.chubarov.tm.exception.entity;

public final class ModelNotFoundException extends AbstractEntityNotFoundException {

    public ModelNotFoundException() {
        super("Error. Model not found.");
    }

}
